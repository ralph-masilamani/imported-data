t#!/bin/bash

read -sp "Enter username: " uname                                                                                
read -sp "Enter password: " pword
read -sp "Enter host: " host
read -sp "Enter database: " db


# Divisions - MP VOTING RECORDS

/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_PARL_Member_Voting_Record --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ukparliament_members_votes/Bill_Division_ID_936.json
/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_PARL_Member_Voting_Record --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ukparliament_members_votes/Bill_Division_ID_937.json
/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_PARL_Member_Voting_Record --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ukparliament_members_votes/Bill_Division_ID_945.json
/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_PARL_Member_Voting_Record --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ukparliament_members_votes/Bill_Division_ID_946.json
