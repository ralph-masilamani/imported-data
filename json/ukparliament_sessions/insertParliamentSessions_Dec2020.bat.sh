#!/bin/bash

read -sp "Enter username: " uname
read -sp "Enter password: " pword
read -sp "Enter host: " host
read -sp "Enter database: " db

/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_PARL_Parliament_Sessions --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ukparliament_sessions/IMP_Parliament_Session_Dec2020.json --jsonArray