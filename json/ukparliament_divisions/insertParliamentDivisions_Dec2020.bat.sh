#!/bin/bash

read -sp "Enter username: " uname
read -sp "Enter password: " pword
read -sp "Enter host: " host
read -sp "Enter database: " db

/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_PARL_Parliament_Divisions --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ukparliament_divisions/IMP_divisions_1_to_500.json --jsonArray

/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_PARL_Parliament_Divisions --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ukparliament_divisions/IMP_divisions_501_to_1000.json --jsonArray