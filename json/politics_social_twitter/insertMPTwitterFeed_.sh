#!/bin/bash

read -sp "Enter username: " uname
read -sp "Enter password: " pword
read -sp "Enter host: " host
read -sp "Enter database: " db


# MPs Twitter Feed - Tweets, Retweets and Replies - Manually sourced from https://www.politics-social.com/
Echo "Loading Tweets..."

/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_POL_SOC_MP_Tweets --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/politics_social_twitter/IMP_MemberTweets_1_24_2021.json --jsonArray