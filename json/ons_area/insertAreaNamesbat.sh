#!/bin/bash

read -sp "Enter username: " uname
read -sp "Enter password: " pword
read -sp "Enter host: " host
read -sp "Enter database: " db


# Country--host $host
Echo "Loading Countries..."
/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_ONS_Country_Name --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ons_area/IMP_Country.json --jsonArray

# Region
Echo "Loading Regions..."
/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_ONS_Region_Name --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ons_area/IMP_Region.json --jsonArray

# Constituency
Echo "Loading Constituencies..."
/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_ONS_Constituency_Name --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ons_area/IMP_Constituency.json --jsonArray

# Local_Authority
Echo "Loading Regions..."
/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_ONS_Local_Authority_Name --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ons_area/IMP_Local_Authority.json --jsonArray

# Ward
Echo "Loading Wards..."
/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_ONS_Ward_Name  --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ons_area/IMP_Ward.json --jsonArray

# Mapping
Echo "Loading Mapping for AREAS..."
/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_ONS_MAP_WARD_AREAS  --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ons_area/IMP_Map_Wards_Area.json --jsonArray