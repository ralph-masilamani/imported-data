#!/bin/bash

read -sp "Enter username: " uname
read -sp "Enter password: " pword
read -sp "Enter host: " host
read -sp "Enter database: " db

# Departments
/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_PARL_Government_Departments --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ukparliament_departments/IMP_Government_Departments.json --jsonArray

# Posts
/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_PARL_Government_Posts --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ukparliament_departments/IMP_Government_Posts.json --jsonArray