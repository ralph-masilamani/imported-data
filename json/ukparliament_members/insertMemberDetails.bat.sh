#!/bin/bash

read -sp "Enter username: " uname
read -sp "Enter password: " pword
read -sp "Enter host: " host
read -sp "Enter database: " db


# Members with detail - From UK Parliament API
/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_PARL_Member --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ukparliament_members/IMP_Member0.json --jsonArray
/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_PARL_Member --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ukparliament_members/IMP_Member20.json --jsonArray
/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_PARL_Member --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ukparliament_members/IMP_Member40.json --jsonArray
/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_PARL_Member --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ukparliament_members/IMP_Member60.json --jsonArray
/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_PARL_Member --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ukparliament_members/IMP_Member80.json --jsonArray
/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_PARL_Member --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ukparliament_members/IMP_Member100.json --jsonArray
/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_PARL_Member --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ukparliament_members/IMP_Member120.json --jsonArray
/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_PARL_Member --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ukparliament_members/IMP_Member140.json --jsonArray
/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_PARL_Member --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ukparliament_members/IMP_Member160.json --jsonArray
/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_PARL_Member --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ukparliament_members/IMP_Member180.json --jsonArray
/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_PARL_Member --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ukparliament_members/IMP_Member200.json --jsonArray
/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_PARL_Member --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ukparliament_members/IMP_Member220.json --jsonArray
/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_PARL_Member --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ukparliament_members/IMP_Member240.json --jsonArray
/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_PARL_Member --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ukparliament_members/IMP_Member260.json --jsonArray
/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_PARL_Member --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ukparliament_members/IMP_Member280.json --jsonArray
/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_PARL_Member --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ukparliament_members/IMP_Member300.json --jsonArray
/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_PARL_Member --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ukparliament_members/IMP_Member320.json --jsonArray
/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_PARL_Member --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ukparliament_members/IMP_Member340.json --jsonArray
/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_PARL_Member --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ukparliament_members/IMP_Member360.json --jsonArray
/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_PARL_Member --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ukparliament_members/IMP_Member380.json --jsonArray
/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_PARL_Member --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ukparliament_members/IMP_Member400.json --jsonArray
/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_PARL_Member --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ukparliament_members/IMP_Member420.json --jsonArray
/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_PARL_Member --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ukparliament_members/IMP_Member440.json --jsonArray
/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_PARL_Member --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ukparliament_members/IMP_Member460.json --jsonArray
/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_PARL_Member --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ukparliament_members/IMP_Member480.json --jsonArray
/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_PARL_Member --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ukparliament_members/IMP_Member500.json --jsonArray
/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_PARL_Member --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ukparliament_members/IMP_Member520.json --jsonArray
/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_PARL_Member --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ukparliament_members/IMP_Member540.json --jsonArray
/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_PARL_Member --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ukparliament_members/IMP_Member560.json --jsonArray
/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_PARL_Member --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ukparliament_members/IMP_Member580.json --jsonArray
/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_PARL_Member --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ukparliament_members/IMP_Member600.json --jsonArray
/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_PARL_Member --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ukparliament_members/IMP_Member620.json --jsonArray
/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_PARL_Member --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ukparliament_members/IMP_Member640.json --jsonArray

