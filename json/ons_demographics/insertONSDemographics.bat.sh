#!/bin/bash

read -sp "Enter username: " uname
read -sp "Enter password: " pword
read -sp "Enter host: " host
read -sp "Enter database: " db


# Homeless by Region 2020
/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_ONS_Homeless_Region --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ons_demographics/IMP_Homeless_Region.json --jsonArray

# Homeless by Local Authority 2020
/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_ONS_Homeless_Local_Authority --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ons_demographics/IMP_Homeless_Local_Authority.json --jsonArray

# Employment by Local Authority 2020
/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_ONS_Employment_Local_Authority --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ons_demographics/IMP_Employment_Local_Authority.json --jsonArray

# Population by Local Authority 2020
/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_ONS_Population_Local_Authority --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ons_demographics/IMP_Population_Local_Authority.json --jsonArray
