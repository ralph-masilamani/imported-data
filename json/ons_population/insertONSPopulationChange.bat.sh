#!/bin/bash

read -sp "Enter username: " uname
read -sp "Enter password: " pword
read -sp "Enter host: " host
read -sp "Enter database: " db


# Population Change by Country, Region 2020

/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_ONS_Population_Change --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ons_population/IMP_PopulationChange2019.json --jsonArray

