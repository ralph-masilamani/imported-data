#!/bin/bash

read -sp "Enter username: " uname
read -sp "Enter password: " pword
read -sp "Enter host: " host
read -sp "Enter database: " db


# Divisions Nov 2020 - Jan 2021 - MP VOTING RECORDS
/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_PARL_Member_Voting_Record --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ukparliament_members_votes_2020/906_2020-11-10T174500_Parliamentary_Constituencies_Bill_Lords_Amendment_6_Motion_to_disagree.json --jsonArray

/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_PARL_Member_Voting_Record --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ukparliament_members_votes_2020/907_2020-11-10T175800_Parliamentary_Constituencies_Bill_Lords_Amendment_7_Motion_to_disagree.json --jsonArray

/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_PARL_Member_Voting_Record --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ukparliament_members_votes_2020/908_2020-11-10T181200_Parliamentary_Constituencies_Bill_Lords_Amendment_8_Motion_to_disagree.json --jsonArray

/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_PARL_Member_Voting_Record --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ukparliament_members_votes_2020/909_2020-11-10T195800_Appointment_of_a_member_to_the_Committee_on_Standards_Amendment_a.json --jsonArray

/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_PARL_Member_Voting_Record --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ukparliament_members_votes_2020/910_2020-11-16T205100_Pension_Schemes_Bill_[Lords]_Report_Stage_New_Clause_1.json --jsonArray

/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_PARL_Member_Voting_Record --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ukparliament_members_votes_2020/911_2020-11-16T210400_Pension_Schemes_Bill_[Lords]_Report_Stage_New_Clause_4.json --jsonArray


/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_PARL_Member_Voting_Record --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ukparliament_members_votes_2020/912_2020-11-16T211700_Pension_Schemes_Bill_[Lords]_Report_Stage_Amendment_1.json --jsonArray

/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_PARL_Member_Voting_Record --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ukparliament_members_votes_2020/913_2020-11-16T213000_Pension_Schemes_Bill_[Lords]_Report_Stage_Amendment_16.json --jsonArray

/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_PARL_Member_Voting_Record --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ukparliament_members_votes_2020/914_2020-11-17T183800_Draft_Road_Vehicle_Carbon_Dioxide_Emission_Performance_Standards_Cars_and_Vans_Amendment_EU_Exit_Regulations_2020.json --jsonArray

/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_PARL_Member_Voting_Record --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ukparliament_members_votes_2020/915_2020-11-25T000000_Draft_European_Union_Withdrawal_Act_2018_Relevant_Court_Retained_EU_Case_Law_Regulations_2020.json --jsonArray

/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_PARL_Member_Voting_Record --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ukparliament_members_votes_2020/916_2020-12-01T190000_Health_Protection_Coronavirus_Restrictions_All_Tiers_England_Regulations_2020.json --jsonArray

/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_PARL_Member_Voting_Record --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ukparliament_members_votes_2020/917_2020-12-02T000000_Draft_Veterinary_Medicines_and_Residues_Amendment_EU_Exit_Regulations_2020.json --jsonArray

/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_PARL_Member_Voting_Record --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ukparliament_members_votes_2020/918_2020-12-07T205500_United_Kingdom_Internal_Market_Bill_Lords_Amendment_1_Motion_to_disagree.json --jsonArray

/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_PARL_Member_Voting_Record --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ukparliament_members_votes_2020/919_2020-12-07T210700_United_Kingdom_Internal_Market_Bill_Lords_Amendment_12_Motion_to_disagree.json --jsonArray

/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_PARL_Member_Voting_Record --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ukparliament_members_votes_2020/920_2020-12-07T212400_United_Kingdom_Internal_Market_Bill_Lords_Amendment_47_Motion_to_disagree.json --jsonArray

/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_PARL_Member_Voting_Record --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ukparliament_members_votes_2020/921_2020-12-07T213400_United_Kingdom_Internal_Market_Bill_Lords_Amendment_48_Motion_to_disagree.json --jsonArray

/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_PARL_Member_Voting_Record --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ukparliament_members_votes_2020/922_2020-12-08T172200_Northern_Ireland_Ways_and_Means_Keir_Starmers_Amendment.json --jsonArray

/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_PARL_Member_Voting_Record --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ukparliament_members_votes_2020/923_2020-12-08T174000_Financial_Assistance_to_Industry.json --jsonArray

/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_PARL_Member_Voting_Record --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ukparliament_members_votes_2020/924_2020-12-09T174100_Taxation_Post-transition_Period_Bill_Committee_of_the_whole_House_Amendment_1.json --jsonArray

/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_PARL_Member_Voting_Record --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ukparliament_members_votes_2020/925_2020-12-09T175400_Immigration_and_Social_Security_Co-ordination_EU_Withdrawal_Act_2020_Consequential_Saving_Transitional_and_Transitory_Provisions_EU_Exit_Regulations_2020.json --jsonArray

/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_PARL_Member_Voting_Record --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ukparliament_members_votes_2020/926_2020-12-10T143300_UK_Internal_Market_Bill_motion_to_disagree_to_Lords_Amendments_1B_1C_and_1D.json --jsonArray

/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_PARL_Member_Voting_Record --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ukparliament_members_votes_2020/927_2020-12-10T144500_UK_Internal_Market_Bill_motion_to_agree_Lords_Amendments_8B_8C_8D_8F_8G_8H_8J_and_8K_disagree_to_8L_insist_on_disagreement_to_13_and_56.json --jsonArray

/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_PARL_Member_Voting_Record --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ukparliament_members_votes_2020/928_2020-12-10T145900_UK_Internal_Market_Bill_motion_to_disagree_to_Lords_Amendments_48B_and_48C.json --jsonArray

/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_PARL_Member_Voting_Record --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ukparliament_members_votes_2020/929_2020-12-15T150700_UK_Internal_Market_Bill_motion_to_disagree_to_Lords_Amendments_1F_1G_1H_1K_1L_and_8M.json --jsonArray

/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_PARL_Member_Voting_Record --file /Users/jamespmcateer/sandbox/workspace/imported-data/json/ukparliament_members_votes_2020/930_2020-12-15T165600_Taxation_Post-transition_Period_Bill_Report_Stage_New_Clause_3.json --jsonArray
