#!/bin/bash

read -sp "Enter username: " uname
read -sp "Enter password: " pword
read -sp "Enter host: " host
read -sp "Enter database: " db


# Combined Authorities - KML files

/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_ONS_KML_Combined_Authorities --file /Users/jamespmcateer/sandbox/workspace/imported-data/kml/kml_combined_authorities/Combined_Authorities1.json
/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_ONS_KML_Combined_Authorities --file /Users/jamespmcateer/sandbox/workspace/imported-data/kml/kml_combined_authorities/Combined_Authorities2.json
/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_ONS_KML_Combined_Authorities --file /Users/jamespmcateer/sandbox/workspace/imported-data/kml/kml_combined_authorities/Combined_Authorities3.json
/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_ONS_KML_Combined_Authorities --file /Users/jamespmcateer/sandbox/workspace/imported-data/kml/kml_combined_authorities/Combined_Authorities4.json
/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_ONS_KML_Combined_Authorities --file /Users/jamespmcateer/sandbox/workspace/imported-data/kml/kml_combined_authorities/Combined_Authorities5.json
/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_ONS_KML_Combined_Authorities --file /Users/jamespmcateer/sandbox/workspace/imported-data/kml/kml_combined_authorities/Combined_Authorities6.json
/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_ONS_KML_Combined_Authorities --file /Users/jamespmcateer/sandbox/workspace/imported-data/kml/kml_combined_authorities/Combined_Authorities7.json
/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_ONS_KML_Combined_Authorities --file /Users/jamespmcateer/sandbox/workspace/imported-data/kml/kml_combined_authorities/Combined_Authorities8.json
/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_ONS_KML_Combined_Authorities --file /Users/jamespmcateer/sandbox/workspace/imported-data/kml/kml_combined_authorities/Combined_Authorities9.json
/usr/local/mongodb/bin/mongoimport --username $uname --password $pword --host $host --db $db --collection IMP_ONS_KML_Combined_Authorities --file /Users/jamespmcateer/sandbox/workspace/imported-data/kml/kml_combined_authorities/Combined_Authorities10.json